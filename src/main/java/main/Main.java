package main;

import com.engine.efficiency.java.rmi.RemoteException;

import javax.rules.*;
import javax.rules.admin.RuleAdministrator;
import javax.rules.admin.RuleExecutionSet;
import javax.rules.admin.RuleExecutionSetCreateException;
import javax.rules.admin.RuleExecutionSetRegisterException;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static main.Measurer.*;

/**
 * Created by Piotr1 on 2016-05-24.
 */
public class Main {
    public static void main (String [] args) throws InterruptedException {
        /*try {
            Class.forName( "org.jruleengine.RuleServiceProviderImpl" );
            File file = new File("example1.xml");
            System.out.println(file.getAbsolutePath());
            InputStream inStream = new FileInputStream( "example1.xml" );
            // Get the rule service provider from the provider manager.
            RuleServiceProvider serviceProvider;
            serviceProvider = RuleServiceProviderManager.getRuleServiceProvider("org.jruleengine");
            // get the RuleAdministrator
            RuleAdministrator ruleAdministrator = serviceProvider.getRuleAdministrator();
            System.out.println("Administration API");
            System.out.println("Acquired RuleAdministrator: " + ruleAdministrator);
            // get an input stream to a test XML ruleset
            // This rule execution set is part of the TCK.
            System.out.println("Acquired InputStream to example1.xml: " + inStream);
            // parse the ruleset from the XML document
            RuleExecutionSet res1 = ruleAdministrator.getLocalRuleExecutionSetProvider( null ).createRuleExecutionSet(inStream, null);
            inStream.close();
            System.out.println("Loaded RuleExecutionSet: " + res1);
            // register the RuleExecutionSet
            String uri = res1.getName(); ruleAdministrator.registerRuleExecutionSet(uri, res1, null );
            System.out.println("Bound RuleExecutionSet to URI: " + uri);
            // Get a RuleRuntime and invoke the rule engine.
            System.out.println("\nRuntime API\n");
            RuleRuntime ruleRuntime = serviceProvider.getRuleRuntime();
            System.out.println("Acquired RuleRuntime: " + ruleRuntime);
            // create a StatefulRuleSession
            StatefulRuleSession statefulRuleSession = (StatefulRuleSession) ruleRuntime.createRuleSession( uri, new HashMap(), RuleRuntime.STATEFUL_SESSION_TYPE );
            System.out.println("Got Stateful Rule Session: " + statefulRuleSession);
            // Add some clauses...
            Customer c = new Customer();
            c.setCreditLimit(371.0);
            Invoice i = new Invoice();
            i.setAmount(200.0);
            statefulRuleSession.addObject(c);
            statefulRuleSession.addObject(i);
            ArrayList<Clause> input = new ArrayList<>();
            input.add(new Clause("Socrate is human"));
            // add an Object to the statefulRuleSession
            statefulRuleSession.addObjects( input );
            System.out.println("Called addObject on Stateful Rule Session: " + statefulRuleSession);
            statefulRuleSession.executeRules();
            System.out.println("Called executeRules");
            // extract the Objects from the statefulRuleSession
            List results = statefulRuleSession.getObjects();
            System.out.println("Result of calling getObjects: " + results.size() + " results.");
            // Loop over the results.
            for (Object obj : results) {
                System.out.println("Clause Found: " + obj.toString());
            }
            // release the statefulRuleSession
            statefulRuleSession.release();
            System.out.println("Released Stateful Rule Session.");

        } catch (ConfigurationException | ClassNotFoundException | FileNotFoundException | RemoteException | RuleExecutionSetCreateException | RuleExecutionSetRegisterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuleSessionCreateException e) {
            e.printStackTrace();
        } catch (RuleExecutionSetNotFoundException e) {
            e.printStackTrace();
        } catch (RuleSessionTypeUnsupportedException e) {
            e.printStackTrace();
        } catch (InvalidRuleSessionException e) {
            e.printStackTrace();
        }*/
        try {
            Class.forName( "org.jruleengine.RuleServiceProviderImpl" );
            String path = "psc-zatrudnienie-ocena_kandydata.pl.xml";
            InputStream inStream = new FileInputStream( new File(path) );
            // Get the rule service provider from the provider manager.
            RuleServiceProvider serviceProvider;
            serviceProvider = RuleServiceProviderManager.getRuleServiceProvider("org.jruleengine");
            // get the RuleAdministrator
            RuleAdministrator ruleAdministrator = serviceProvider.getRuleAdministrator();
//            textView.setText(textView.getText().toString() + "\n\nDEBUG: " + "Administration API");
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Acquired RuleAdministrator: " + ruleAdministrator);
            // get an input stream to a test XML ruleset
            // This rule execution set is part of the TCK.
            // InputStream inStream = new FileInputStream( "example3.xml" );
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Acquired InputStream to "+path+": " + inStream);
            // parse the ruleset from the XML document
            RuleExecutionSet res1 = ruleAdministrator.getLocalRuleExecutionSetProvider( null ).createRuleExecutionSet(inStream, null);
            inStream.close();
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Loaded RuleExecutionSet: " + res1);
            // register the RuleExecutionSet
            String uri = res1.getName();
            ruleAdministrator.registerRuleExecutionSet(uri, res1, null );
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Bound RuleExecutionSet to URI: " + uri);
            // Get a RuleRuntime and invoke the rule engine.
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "\nRuntime API\n");
            RuleRuntime ruleRuntime = serviceProvider.getRuleRuntime();
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Acquired RuleRuntime: " + ruleRuntime);
            // create a StatefulRuleSession
            StatefulRuleSession statefulRuleSession = (StatefulRuleSession) ruleRuntime.createRuleSession( uri, new HashMap(), RuleRuntime.STATEFUL_SESSION_TYPE );
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Got Stateful Rule Session: " + statefulRuleSession);
            // Add some clauses...

            ocenakandydata _ocenakandydata = new ocenakandydata();
            _ocenakandydata.setOcenaRozmowy(2.0);
            _ocenakandydata.setOcenaKwalifikacji(5.0);
            _ocenakandydata.setOcenaTestow(2.0);
            statefulRuleSession.addObject(_ocenakandydata);
            // add an Object to the statefulRuleSession
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Called addObject on Stateful Rule Session: " + statefulRuleSession);
            new Thread(new Measurer()).start();
            while (cpu == -1) {
                System.out.println("Waiting for measuring thread...");
                Thread.sleep(3000);
            }
            BufferedWriter bw = new BufferedWriter(new FileWriter("results.txt",true));
            Date start = new Date();
            statefulRuleSession.executeRules();
            Date end = new Date();

            Measurer.ifContinue = false;

            Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            Double [] proc = processorUsage.toArray(new Double[processorUsage.size()]);
            double minProc = proc[0];
            double maxProc = proc[0];
            for (double p : proc) {
                if (p < minProc) minProc = p;
                if (p > maxProc) maxProc = p;
            }
            try {
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB , Processor diff: ");
                bw.write(String.valueOf(maxProc-minProc));
                bw.write(" % , Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms \n-------\n");
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Called executeRules");
            // extract the Objects from the statefulRuleSession
            List results = statefulRuleSession.getObjects();
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Result of calling getObjects: " + results.size() + " results.");
            // Loop over the results.
            for (Object obj : results) {
                System.out.println("\nDEBUG: " + "Clause Found: " + obj.toString());
            }
            // release the statefulRuleSession
            statefulRuleSession.release();
//            textView.setText(textView.getText().toString() + "\nDEBUG: " + "Released Stateful Rule Session.");

        } catch (ConfigurationException | ClassNotFoundException | FileNotFoundException | RemoteException | RuleExecutionSetCreateException | RuleExecutionSetRegisterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuleSessionCreateException e) {
            e.printStackTrace();
        } catch (RuleExecutionSetNotFoundException e) {
            e.printStackTrace();
        } catch (RuleSessionTypeUnsupportedException e) {
            e.printStackTrace();
        } catch (InvalidRuleSessionException e) {
            e.printStackTrace();
        }
    }
}
