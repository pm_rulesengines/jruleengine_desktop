package main;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Piotr1 on 2016-08-03.
 */
class Measurer implements Runnable {

    static List<Long> memoryUsage = new LinkedList<>();
    static List<Double> processorUsage = new LinkedList<>();
    static boolean ifContinue = true;
    static double cpu = -1;

    @Override
    public void run() {
        while (ifContinue) {
            memoryUsage.add(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
            try {
                cpu = getProcessCpuLoad();
                if (cpu != -1)
                    processorUsage.add(cpu);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //Runtime.getRuntime().
        }
    }

    private static double getProcessCpuLoad() throws Exception {

        MBeanServer mbs    = ManagementFactory.getPlatformMBeanServer();
        ObjectName name    = ObjectName.getInstance("java.lang:type=OperatingSystem");
        AttributeList list = mbs.getAttributes(name, new String[]{ "ProcessCpuLoad" });

        if (list.isEmpty())     return Double.NaN;

        Attribute att = (Attribute)list.get(0);
        Double value  = (Double)att.getValue();

        // usually takes a couple of seconds before we get real values
        if (value == -1.0)      return Double.NaN;
        // returns a percentage value with 1 decimal point precision
        return ((int)(value * 1000) / 10.0);
    }
}
